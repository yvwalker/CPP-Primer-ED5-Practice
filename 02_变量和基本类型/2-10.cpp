#include <iostream>

// global variation is 0;
std::string global_str;
int global_int;
int main()
{
    // local variation is undefined;
    int local_int;
    std::string local_str;

    std::cout << "global_str is: " << global_str << std::endl
              << "global_int is: " << global_int << std::endl
              << "local_int is: " << local_int << std::endl
              << "local_str is: " << local_str << std::endl;

    return 0;
}