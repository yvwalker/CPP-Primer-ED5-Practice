#include <iostream>
using std::endl;

int main()
{
    std::cout << "int is:" << sizeof(int) << endl;
    std::cout << "long is:" << sizeof(long) << endl;
    std::cout << "long long is:" << sizeof(long long) << endl;
    std::cout << "short is:" << sizeof(short) << endl;

    std::cout << "float is:" << sizeof(float) << endl;
    std::cout << "double is:" << sizeof(double) << endl;

    return 0;
}