#include <iostream>

int main()
{
    int a[2] = {0, 1};
    int *p1 = a;
    int *p2 = p1 + 1;
    *p1 = 3;

    std::cout << a[0] << " " << a[1] << std::endl;

    return 0;
}