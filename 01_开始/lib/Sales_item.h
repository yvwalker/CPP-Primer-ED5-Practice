#include <iostream>

class Sales_item
{
  public:
    char ISBN[14];
    double price;
    double number;
    double average;

    Sales_item() {}
    std::istream operator>>(Sales_item a);
    ~Sales_item() {}
}
