#include <iostream>
using std::cin;
using std::cout;
using std::endl;

int main(int argc, char const *argv[])
{
    cout << "Enter two number: ";

    int a, b;
    cin >> a >> b;
    cout << "The ";
    cout << a;
    cout << " multi ";
    cout << b;
    cout << " is ";
    cout << a * b;
    cout << endl;

    return 0;
}
