#include <iostream>
using std::cin;
using std::cout;
using std::endl;

int main()
{
    // 1.10
    int sum = 0;
    for (int i = 50; i < 100; i++)
    {
        sum = sum + i;
    }
    cout << "50~100: " << sum << endl;

    // 1.10
    for (int i = 10; i > 0; i--)
    {
        cout << "num is: " << i << endl;
    }

    // 1.11
    int a, b;
    cout << "Enter two numbers: ";
    cin >> a >> b;

    for (int i = a; i < b; i++)
    {
        cout << "num is: " << i << endl;
    }
}