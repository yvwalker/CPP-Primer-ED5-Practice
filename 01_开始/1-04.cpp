#include <iostream>
using std::cin;
using std::cout;
using std::endl;

int main(int argc, char const *argv[])
{
    cout << "Enter two number: ";

    int a, b;
    cin >> a >> b;
    cout << "The " << a << " multi " << b << " is " << a * b << endl;

    return 0;
}
