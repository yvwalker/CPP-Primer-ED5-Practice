#include <cstdio>
#include <iostream>

int main()
{
    // Only can use in C++.
    std::cout << "Hello, World" << std::endl;

    // Can use in C and C++.
    printf("Hello, World\n");
    fprintf(stdout, "Hello, World\n");
}
