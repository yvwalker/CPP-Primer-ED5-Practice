#include <iostream>

int main()
{
    std::cout << "Enter two number: ";
    int a, b;
    std::cin >> a >> b;

    int max, min;

    if (a > b)
    {
        min = b;
        max = a;
    }
    else
    {
        min = a;
        max = b;
    }

    while (min < max)
    {
        std::cout << "Num is: " << min << std::endl;
        min++;
    }

    return 0;
}